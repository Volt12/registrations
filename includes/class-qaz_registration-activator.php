<?php

/**
 * Fired during plugin activation
 *
 * @link       nosite.com
 * @since      1.0.0
 *
 * @package    Qaz_registration
 * @subpackage Qaz_registration/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Qaz_registration
 * @subpackage Qaz_registration/includes
 * @author     Volodymyr Dhzychko <dhzychko@gmail.com>
 */
class Qaz_registration_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
		$db_table_name = $wpdb->prefix . 'qaz_calendar';


		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $db_table_name (
		  id mediumint(9) NOT NULL AUTO_INCREMENT,
		  start_time datetime DEFAULT '0000-00-00 00:00' NULL,
		  end_time datetime DEFAULT '0000-00-00 00:00' NULL,
		  customer_id int(9) DEFAULT '0' NULL,
		  provider_id int(9) DEFAULT '0' NULL,
		  PRIMARY KEY  (id)
		) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

		if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'regestration'", 'ARRAY_A' ) ) {
     
			$current_user = wp_get_current_user();
			
			// create post object
			$page = array(
			  'post_title'  => __( 'Registration' ),
			  'post_status' => 'publish',
			  'post_author' => $current_user->ID,
			  'post_type'   => 'page',
			);
			
			// insert the post into the database
			wp_insert_post( $page );
		  }
	}

}
