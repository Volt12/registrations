<?php

/**
 * Fired during plugin deactivation
 *
 * @link       nosite.com
 * @since      1.0.0
 *
 * @package    Qaz_registration
 * @subpackage Qaz_registration/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Qaz_registration
 * @subpackage Qaz_registration/includes
 * @author     Volodymyr Dhzychko <dhzychko@gmail.com>
 */
class Qaz_registration_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
